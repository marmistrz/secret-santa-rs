# secret-santa-rs

Yes, this modification of the original code is bad and dirty. Look at the source code at your own risk. :P

## Installation

- Install `cargo`.
- Run `cargo install secret-santa-rs`.

## Usage

- Create the file `names.txt` containing names.
  - Example file contents:
    ```
    John Smith
    Liam Anderson
    Emma Jones
    Bob
    ```
- Run `secret-santa-rs` in the directory containing the names file.
  The program will output a list of who buys for who.
  People with the same last name will not be paired with each other.
- If the program cannot find a valid pair for someone, it will output `None`
  (e.g. "Bob buys for None").
  In some cases, running the program again will resolve this issue due to
  different randomness.
