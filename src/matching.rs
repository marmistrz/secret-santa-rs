//! Deterministic matching code
use std::collections::{HashMap, HashSet};

use rusty_santa::Group;

use crate::person::Person;

pub(crate) type Matching = HashMap<String, HashSet<String>>;

pub(crate) fn santa_match(people: &[Person], num_gifts: usize) -> Matching {
    let mut matching: Matching = Matching::default();
    let mut group = Group::new();
    for person in people {
        group.add(person.clone());
    }
    for _ in 0..num_gifts {
        let round_matching = group.assign().expect("matching failed");
        for (k, v) in round_matching {
            let entry = matching.entry(k.clone()).or_default();
            entry.insert(v.clone());
            group.exclude(k, v);
        }
    }
    matching
}

#[cfg(test)]
mod test {
    use super::*;
    use proptest::proptest;
    use rand::Rng;

    #[test]
    fn test_match() {
        let people: Vec<_> = ["Joe", "Jane", "Jude", "Steve"]
            .into_iter()
            .map(Into::into)
            .collect();
        let num_gifts = 2;

        println!("{people:?} -> {num_gifts}");
        let matching = santa_match(&people, num_gifts);
        for (k, v) in &matching {
            assert_eq!(v.len(), num_gifts, "wrong number of gifts");
            assert!(!v.contains(k), "do not give gift to yourself");
        }
        for person in &people {
            let gifts_received: usize = matching
                .values()
                .map(|v| usize::from(v.contains(person)))
                .sum();
            assert_eq!(gifts_received, num_gifts, "everyone should get {num_gifts}");
        }
    }

    proptest! {
        #[allow(clippy::pedantic)]
        #[test]
        fn test_prop(num_people in 2..20) {
            let num_people: usize = num_people as usize;
            let people: Vec<_> = (0..num_people).map(|a| a.to_string()).collect();
            let num_gifts = rand::thread_rng().gen_range(1..num_people);
            let matching = santa_match(&people, num_gifts);

            for (k, v) in &matching {
                assert_eq!(v.len(), num_gifts, "wrong number of gifts");
                assert!(!v.contains(k), "do not give gift to yourself");
            }
            for person in &people {
                let gifts_received: usize = matching
                    .values()
                    .map(|v| usize::from(v.contains(person)))
                    .sum();
                assert_eq!(gifts_received, num_gifts, "everyone should get {num_gifts}");
            }
        }
    }
}
