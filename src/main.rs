//! A generator for who buys for who in a secret santa.

#![warn(missing_debug_implementations, clippy::unwrap_used, clippy::pedantic)]

mod fs;
mod matching;
mod person;

use crate::{fs::from_lines, person::Person};
use clap::Parser;
use itertools::Itertools;
use matching::santa_match;
use std::path::Path;

/// Generator for who buys for who in a secret santa.
#[derive(Parser)]
#[clap(version)]
struct Cli {
    /// The file containing the list of names.
    #[clap(default_value_t = String::from("names.txt"))]
    filepath: String,
    /// Number of gifts per person.
    #[clap(short, long, default_value_t = 1)]
    gifts: usize,
}

fn main() -> anyhow::Result<()> {
    let cli = Cli::parse();

    // Get list of people.
    let path = Path::new(&cli.filepath);
    let people: Vec<Person> = from_lines(path)?;
    let matching = santa_match(&people, cli.gifts);
    // Generate and print the pairs.
    // let rng = |range| thread_rng().gen_range(range);
    // let pairs = LinkVec::<Person>::new(people, cli.retries, &rng)
    //     .expect("rand shouldn't produce out of range values");
    // let pairs = LinkedPeople::from(&pairs);
    for (person, recvrs) in matching {
        let recvrs = recvrs.into_iter().join(", ");
        let b64 = base64::encode(recvrs);
        println!("{person} gives to: {b64}");
        println!(
            "For decoding, just visit https://base64decode.org and copy-paste the scrambled output.\n"
        );
    }

    Ok(())
}
