//! File system related utilities.

use std::{fs::read_to_string, path::Path};

/// Read lines in a file and convert them to a vec of T.
/// Skips empty lines.
pub fn from_lines<T>(filepath: &Path) -> anyhow::Result<Vec<T>>
where
    T: From<String>,
{
    let contents = read_to_string(filepath)?;
    let vec = contents
        .lines()
        .filter(|line| !line.is_empty())
        .map(|line| line.to_owned().into())
        .collect::<Vec<_>>();
    Ok(vec)
}
